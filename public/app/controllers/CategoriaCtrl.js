'use strict';
app.controller("CategoriaCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {

    $scope.categorias = [];
    $scope.icons = ["account_balance", "card_giftcard", "credit_card", "face", "explore", "flight", 
                    "home", "important_devices", "pets", "call", "shopping_cart", "attach_money",
                    "format_paint", "nature_people", "directions_bus", "directions_car", "restaurant", "local_hospital",
                    "fitness_center", "school", "wc", "help_outline"];
    
    $scope.salvar = function(item){
        
        $scope.item = angular.copy(item);
        
        if ($scope.item != undefined)
        {
            
            dbFactory.setCategoria($scope.item).then(function (data) {

                swal("Sucesso", "Registro inserido!", "success");
                $scope.limpar();
                $scope.init();
            }, function (result) {
                swal("Erro", result, "error");
            });    
        }
    };
    
    $scope.limpar = function(){
        $scope.item = {};
    }
        
    $scope.init = function(){
        $scope.isLoading = true;
        dbFactory.getCategorias().then(function(data){ 
            
            $scope.categorias = data; 
            $scope.isLoading = false;
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });
    };
    $scope.init();
    
}]);