'use strict';
app.controller("IndexCtrl", ['$scope', '$rootScope', '$state', '$stateParams', 'dbFactory', function ($scope, $rootScope, $state, $stateParams, dbFactory) {

    $rootScope.anoSelecionado = new Date().getFullYear();
    $rootScope.mesSelecionado = new Date().getMonth()+1;
    $rootScope.nomeMesSelecionado = dbFactory.getNomeMes(''+$rootScope.mesSelecionado+'');

    $scope.meses = [
      {id: 1, descricao: 'Janeiro'},
      {id: 2, descricao: 'Fevereiro'},
      {id: 3, descricao: 'Março'},
      {id: 4, descricao: 'Abril'},
      {id: 5, descricao: 'Maio'},
      {id: 6, descricao: 'Junho'},
      {id: 7, descricao: 'Julho'},
      {id: 8, descricao: 'Agosto'},
      {id: 9, descricao: 'Setembro'},
      {id: 10, descricao: 'Outubro'},
      {id: 11, descricao: 'Novembro'},
      {id: 12, descricao: 'Dezembro'}
  ];
    
    $scope.changeAnoSelecionado = function(ano){
        $rootScope.anoSelecionado = ano;
        $state.go('home');
        
        var elem = document.querySelector('.mdl-menu__container');
        elem.className = 'mdl-menu__container is-upgraded';
    }

    $scope.selectedIndex = $rootScope.mesSelecionado;
    $scope.carregaLancamentos = function (mes) {
        
        $scope.selectedIndex = mes.id;
        $rootScope.mesSelecionado = mes.id;
        $rootScope.nomeMesSelecionado = dbFactory.getNomeMes(''+$rootScope.mesSelecionado+'');
        
        $state.go('lancamentos');
    }

    $scope.changeStateMfb = function(){

    if ($scope.stateMenu == 'closed'){
      $scope.stateMenu = 'open';
    }
    else{
      $scope.stateMenu = 'closed';
    }
  }

    $scope.closeBarActions = function () {
    $rootScope.showBarActions = false;
    $state.go('home');
  }

    $rootScope.carregaAnosExistentes = function(){
    
      dbFactory.getAnosExistentes().then(function(data){ 
        $scope.anos = data; 
      },
      function(result){
        swal("Erro", result, "error");
      });  
  };

    $scope.init = function(){
        $rootScope.carregaAnosExistentes();

        $rootScope.showBarActions = false;

        setTimeout(function(){
          var elem = document.querySelector('.mdl-layout__tab-bar-container');
          elem.className = 'mdl-layout__tab-bar-container mdl-cell--hide-phone mdl-cell--hide-tablet';
        }, 200);

    }
    $scope.init();


}]);
