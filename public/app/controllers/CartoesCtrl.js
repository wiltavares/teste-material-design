'use strict';
app.controller("CartoesCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {
    $scope.item = {};
    $scope.cartoes = [];
    $scope.icons = [
            {descricao: 'American Express', icon: 'fa-cc-amex', valor: 'amex'}, 
            {descricao: "MasterCard", icon: "fa-cc-mastercard", valor: "mastercard"}, 
            {descricao: "Visa", icon: "fa-cc-visa", valor: "visa"}, 
            {descricao: "Diners Club", icon: "fa-cc-diners-club", valor: "diners"}, 
            {descricao: "PayPal", icon: "fa-cc-paypal", valor: "paypal"},
            {descricao: "Outros", icon: "fa-credit-card", valor: "outros"}
    ];
    
    $scope.limpar = function(){
        $scope.item = {};
    }
    
    $scope.salvar = function(){
        debugger;
        
        if ($scope.item != undefined)
        {
            dbFactory.setCartao($scope.item).then(function (data) {

                swal("Sucesso", "Registro inserido!", "success");
                $scope.limpar();
                $scope.init();
            }, function (result) {
                swal("Erro", result, "error");
            });    
        }
        
    }
    
    
    $scope.init = function(){
        $scope.isLoading = true;
        dbFactory.getCartoes().then(function(data){ 
            
            $scope.cartoes = data; 
            $scope.isLoading = false;
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });
    };
    $scope.init();
}]);