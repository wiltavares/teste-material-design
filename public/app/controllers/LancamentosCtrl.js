app.controller("LancamentosCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {
    
    $scope.lancamentos = [];
    $scope.fatura = [];
    $scope.cartoes = [];
    $scope.receitasAbertas = false;
    $scope.despesasAbertas = false;
    
    $scope.totalizaPorTipo = function(lancamentos, tipo, soAbertos){
        var sum = 0;
        
        lancamentos.forEach(function(lancamento){
            if (soAbertos == true)
            {
                if (lancamento.tipo == tipo && lancamento.pagamentoRealizado == false)
                    sum = sum + lancamento.valor;
            }
            else
            {
                if (lancamento.tipo == tipo)
                    sum = sum + lancamento.valor;            
            }
           
        });
        
        return sum;
    }
    
    $scope.totalizaConsolidadosGeral = function(){
        var existeConta = false;
        var item = {entrada: 0, saida: 0, total: 0, entradaFechada: 0, saidaFechada: 0, totalFechado: 0, entradaAberta: 0, saidaAberta: 0, totalAberto: 0};
        $scope.consolidadosGeral = [];
        $scope.totaisConsolidadosGeral = {entrada: 0, saida: 0, total: 0, entradaFechada: 0, saidaFechada: 0, totalFechado: 0, entradaAberta: 0, saidaAberta: 0, totalAberto: 0};
        
        $scope.lancamentos.forEach(function(lancamento){
            
            if ($scope.consolidadosGeral.length > 0){
                
                $scope.consolidadosGeral.forEach(function(consolidado){
                    
                    if (lancamento.conta == consolidado.conta)
                    {
                        if (lancamento.tipo == 'receita')
                        {
                            consolidado.entrada += lancamento.valor;
                            
                            if (lancamento.pagamentoRealizado == true)
                            {
                                consolidado.entradaFechada += lancamento.valor; 
                                consolidado.totalFechado = consolidado.entradaFechada - consolidado.saidaFechada;
                            }
                            else
                            {
                                consolidado.entradaAberta += lancamento.valor; 
                                consolidado.totalAberto = consolidado.entradaAberta - consolidado.saidaAberta; 
                            }
                        }
                        else
                        {
                            consolidado.saida += lancamento.valor; 
                            
                            if (lancamento.pagamentoRealizado == true)
                            {
                                consolidado.saidaFechada += lancamento.valor; 
                                consolidado.totalFechado = consolidado.entradaFechada - consolidado.saidaFechada;
                            }
                            else
                            {
                                consolidado.saidaAberta += lancamento.valor; 
                                consolidado.totalAberto = consolidado.entradaAberta - consolidado.saidaAberta; 
                            }
                        }
                        
                        consolidado.total =  consolidado.entrada -  consolidado.saida;
                        existeConta = true;
                    }
                });
                    
                if (!existeConta)
                {
                    
                    item.conta = lancamento.conta;
                    
                    if (lancamento.tipo == 'receita')
                    {
                        item.entrada = lancamento.valor;
                        
                        if (lancamento.pagamentoRealizado == true)
                        {
                            item.entradaFechada = lancamento.valor; 
                            item.totalFechado = item.entradaFechada - item.saidaFechada;
                        }
                        else
                        {
                            item.entradaAberta += lancamento.valor; 
                            item.totalAberto = item.entradaAberta - item.saidaAberta; 
                        }
                    }
                    else
                    {
                        item.saida = lancamento.valor; 
                        
                        if (lancamento.pagamentoRealizado == true)
                        {
                            item.saidaFechada = lancamento.valor; 
                            item.totalFechado = item.entradaFechada - item.saidaFechada;
                        }
                        else
                        {
                            item.saidaAberta += lancamento.valor; 
                            item.totalAberto = item.entradaAberta - item.saidaAberta; 
                        }
                    }
                    
                    item.total = item.entrada - item.saida;

                    $scope.consolidadosGeral.push(item);
                    item = {entrada: 0, saida: 0, total: 0, entradaFechada: 0, saidaFechada: 0, totalFechado: 0, entradaAberta: 0, saidaAberta: 0, totalAberto: 0};    
                }
                    
                existeConta = false;                    
                    
            }         
            else
            {
                
                item.conta = lancamento.conta;
                
                if (lancamento.tipo == 'receita')
                {
                    item.entrada = lancamento.valor; 
                    
                    if (lancamento.pagamentoRealizado == true)
                    {
                        item.entradaFechada = lancamento.valor; 
                        item.totalFechado = item.entradaFechada - item.saidaFechada;
                    }
                    else
                    {
                        item.entradaAberta += lancamento.valor; 
                        item.totalAberto = item.entradaAberta - item.saidaAberta; 
                    }
                }else
                {
                    item.saida = lancamento.valor;   
                    
                    if (lancamento.pagamentoRealizado == true)
                    {
                        item.saidaFechada = lancamento.valor; 
                        item.totalFechado = item.entradaFechada - item.saidaFechada;
                    }
                    else
                    {
                        item.saidaAberta += lancamento.valor; 
                        item.totalAberto = item.entradaAberta - item.saidaAberta; 
                    }
                }
                
                item.total = item.entrada - item.saida;
                
                $scope.consolidadosGeral.push(item);
                item = {entrada: 0, saida: 0, total: 0, entradaFechada: 0, saidaFechada: 0, totalFechado: 0, entradaAberta: 0, saidaAberta: 0, totalAberto: 0};
            }
            
        });
        
        $scope.consolidadosGeral.forEach(function(consolidadoGeral){
            $scope.totaisConsolidadosGeral.entrada += consolidadoGeral.entrada;
            $scope.totaisConsolidadosGeral.saida += consolidadoGeral.saida;
            $scope.totaisConsolidadosGeral.total += consolidadoGeral.total;
            
            $scope.totaisConsolidadosGeral.entradaFechada += consolidadoGeral.entradaFechada;
            $scope.totaisConsolidadosGeral.saidaFechada += consolidadoGeral.saidaFechada;
            $scope.totaisConsolidadosGeral.totalFechado += consolidadoGeral.totalFechado;
            
            $scope.totaisConsolidadosGeral.entradaAberta += consolidadoGeral.entradaAberta;
            $scope.totaisConsolidadosGeral.saidaAberta += consolidadoGeral.saidaAberta;
            $scope.totaisConsolidadosGeral.totalAberto += consolidadoGeral.totalAberto;
        });
    }
    
    $scope.carregaSaldosProjetados = function(){
        var existeConta = false;
        var mes = $rootScope.mesSelecionado == 1 ? 12 : $rootScope.mesSelecionado - 1;
        var ano = $rootScope.mesSelecionado == 1 ? $rootScope.anoSelecionado - 1 : $rootScope.anoSelecionado;
        var item = {resultado: 0, anterior: 0, total: 0};
        $scope.totalSaldoProjetado = {resultado: 0, anterior: 0, total: 0};
        $scope.saldosProjetados = [];
        
        $scope.consolidadosGeral.forEach(function(consolidado)
        {
            item.conta = consolidado.conta
            item.resultado = consolidado.total;
            item.total = item.anterior + item.resultado; 
            
            $scope.totalSaldoProjetado.resultado += item.resultado;
            $scope.totalSaldoProjetado.total += item.total
            
            $scope.saldosProjetados.push(item);
            item = {resultado: 0, anterior: 0, total: 0};
        });
        
        dbFactory.getLancamentosDetalhes(ano, mes).then(function(lancamentos){
            
            lancamentos.forEach(function(lancamento){
                
                $scope.saldosProjetados.forEach(function(projetado){
                    
                    if (projetado.conta == lancamento.conta)
                    {
                        if (lancamento.tipo == 'receita')
                        {
                            projetado.anterior += lancamento.valor;
                            $scope.totalSaldoProjetado.anterior += lancamento.valor;
                        }                                                        
                        else
                        {
                            projetado.anterior -= lancamento.valor;
                            $scope.totalSaldoProjetado.anterior -= lancamento.valor;
                        }                           
                        
                        projetado.total = projetado.anterior + projetado.resultado;
                        $scope.totalSaldoProjetado.total = $scope.totalSaldoProjetado.anterior + $scope.totalSaldoProjetado.resultado;
                        
                        existeConta = true;
                    }
                }); 
                
                if (!existeConta)
                {
                    
                    item.conta = lancamento.conta;
                    item.resultado = 0;
                    item.anterior = lancamento.valor;
                    item.total = item.resultado + item.anterior;
                    
                    $scope.saldosProjetados.push(item);
                    
                    $scope.totalSaldoProjetado.anterior += item.anterior;
                    $scope.totalSaldoProjetado.total = $scope.totalSaldoProjetado.anterior + $scope.totalSaldoProjetado.resultado;
                }
                existeConta = false;
            });
        },
        function(result){
            swal("Erro", result, "error");    
        }); 
        
    }
    
    $scope.totalizaPorCategoria = function(){
       
        $scope.pieDataDespesa = [];
        $scope.pieDataReceita = [];
        var item = {};
        var existeConta = false;
        
        $scope.lancamentos.forEach(function(lancamento){
            
            if (lancamento.tipo == 'receita')
            {
                if ($scope.pieDataReceita.length > 0)
                {
                    $scope.pieDataReceita.forEach(function(receita){
                        if (receita.name == lancamento.categoria.descricao)
                        {
                            receita.y += lancamento.valor;
                            existeConta = true;
                        }
                    });
                    
                    if (!existeConta)
                    {
                        item.name = lancamento.categoria.descricao;
                        item.y = lancamento.valor;

                        $scope.pieDataReceita.push(item);
                        item = {};        
                    }
                    existeConta = false;
                }   
                else
                {
                    item.name = lancamento.categoria.descricao;
                    item.y = lancamento.valor;
                    item.sliced= true; 
                    item.selected= true;
                    
                    $scope.pieDataReceita.push(item);
                    item = {};
                }
            }
            else
            {
                if ($scope.pieDataDespesa.length > 0)
                {
                    $scope.pieDataDespesa.forEach(function(despesa){
                        if (despesa.name == lancamento.categoria.descricao)
                        {
                            despesa.y += lancamento.valor;
                            existeConta = true;
                        }
                    });
                    
                    if (!existeConta)
                    {
                        item.name = lancamento.categoria.descricao;
                        item.y = lancamento.valor;

                        $scope.pieDataDespesa.push(item);
                        item = {};        
                    }
                    existeConta = false;
                }   
                else
                {
                    item.name = lancamento.categoria.descricao;
                    item.y = lancamento.valor;
                    item.sliced= true; 
                    item.selected= true;
                    
                    $scope.pieDataDespesa.push(item);
                    item = {};
                }
            }
        });
        
        $scope.pieDataDespesa.forEach(function(despesa){
            despesa.porcentagem = parseFloat(((despesa.y / $scope.totaisConsolidadosGeral.saida) * 100).toFixed(2));
        });
        
        $scope.pieDataReceita.forEach(function(receita){
            receita.porcentagem = parseFloat(((receita.y / $scope.totaisConsolidadosGeral.entrada) * 100).toFixed(2));    
        });
    }
    
    $scope.totalizaPorConta = function(){
       
        $scope.pieDataContaDespesa = [];
        $scope.pieDataContaReceita = [];
        var item = {};
        var existeConta = false;
        
        $scope.lancamentos.forEach(function(lancamento){
            
            if (lancamento.tipo == 'receita')
            {
                if ($scope.pieDataContaReceita.length > 0)
                {
                    $scope.pieDataContaReceita.forEach(function(receita){
                        if (receita.name == lancamento.conta)
                        {
                            receita.y += lancamento.valor;
                            existeConta = true;
                        }
                    });
                    
                    if (!existeConta)
                    {
                        item.name = lancamento.conta;
                        item.y = lancamento.valor;

                        $scope.pieDataContaReceita.push(item);
                        item = {};        
                    }
                    existeConta = false;
                }   
                else
                {
                    item.name = lancamento.conta;
                    item.y = lancamento.valor;
                    item.sliced= true; 
                    item.selected= true;
                    
                    $scope.pieDataContaReceita.push(item);
                    item = {};
                }
            }
            else
            {
                if ($scope.pieDataContaDespesa.length > 0)
                {
                    $scope.pieDataContaDespesa.forEach(function(despesa){
                        if (despesa.name == lancamento.conta)
                        {
                            despesa.y += lancamento.valor;
                            existeConta = true;
                        }
                    });
                    
                    if (!existeConta)
                    {
                        item.name = lancamento.conta;
                        item.y = lancamento.valor;

                        $scope.pieDataContaDespesa.push(item);
                        item = {};        
                    }
                    existeConta = false;
                }   
                else
                {
                    item.name = lancamento.conta;
                    item.y = lancamento.valor;
                    item.sliced= true; 
                    item.selected= true;
                    
                    $scope.pieDataContaDespesa.push(item);
                    item = {};
                }
            }
        });
        
        $scope.pieDataContaDespesa.forEach(function(despesa){
            despesa.porcentagem = parseFloat(((despesa.y / $scope.totaisConsolidadosGeral.saida) * 100).toFixed(2));
            
        });
        
        $scope.pieDataContaReceita.forEach(function(receita){
            receita.porcentagem = parseFloat(((receita.y / $scope.totaisConsolidadosGeral.entrada) * 100).toFixed(2));    
        });
    }
    
    $scope.getDiasLancamentos = function(){
        $scope.diasLancamentos = [];
        var dia;
        
        $scope.lancamentos.forEach(function(lancamento){
            
            dia = new Date(lancamento.vencimento).getDate();
            
            if ($scope.diasLancamentos.indexOf(dia) == -1)
            {
               $scope.diasLancamentos.push(dia); 
            }
        });
    }
     
    $scope.getValorPorDia = function(){
        $scope.valoresPorDia = [];
        var valor = 0;
        var valorDiaAnterior = 0;
        
        $scope.diasLancamentos.forEach(function(dia){
            $scope.lancamentos.forEach(function(lancamento){
                diaVencimento = new Date(lancamento.vencimento).getDate();
                
                if (diaVencimento == dia)
                {
                    if (lancamento.tipo == 'receita')
                        valor += lancamento.valor;
                    else
                        valor -= lancamento.valor;
                }    
            }); 
            valor += valorDiaAnterior;
            
            $scope.valoresPorDia.push(parseFloat(valor.toFixed(2)));
            valorDiaAnterior = valor;
            valor = 0;
            
        });   
    }
    
    $scope.configChartMovimentacao = function(){
        $scope.chartOptions = new Highcharts.Chart({
            chart: {
                renderTo: 'chartMovimentacao',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
        title: { text: 'Fluxo do mês' },
        xAxis: { categories: $scope.diasLancamentos },
         yAxis: {
                title: { text: 'Valores (R$)' },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#fff'
                }]
         },
         credits: { enabled: false },
         series: [{
             name: 'Resultado do dia',
             data: $scope.valoresPorDia
        }]
        });    
    }

    $scope.configChartReceita = new Highcharts.Chart({
        chart: {
          renderTo: 'chartReceitas',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
        },
        title: {
          text: 'Receitas por Categorias'
        },
        tooltip: {
          pointFormat: '{series.name} <b>R$ {point.y}</b>',
          percentageDecimals: 1
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: '#000000',
              formatter: function () {
                return '<b>' + this.point.name + '</b>: % ' + this.point.porcentagem + ' ';
              }
            }
          }
        },
        credits: { enabled: false },
        series: [{
          type: 'pie',
          name: 'Valor: ',
          data: $scope.pieDataReceita
        }]
      });
    
    $scope.configChartDespesa = new Highcharts.Chart({
        chart: {
          renderTo: 'chartDespesas',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
        },
        title: {
          text: 'Despesas por Categorias'
        },
        tooltip: {
          pointFormat: '{series.name} <b>R$ {point.y}</b>',
          percentageDecimals: 1
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: '#000000',
              formatter: function () {
                return '<b>' + this.point.name + '</b>: % ' + this.point.porcentagem + ' ';
              }
            }
          }
        },
        credits: { enabled: false },
        series: [{
          type: 'pie',
          name: 'Valor: ',
          data: $scope.pieDataDespesa
        }]
    });
    
    $scope.configChartContaReceita = new Highcharts.Chart({
        chart: {
          renderTo: 'chartReceitasConta',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
        },
        title: {
          text: 'Receitas por Contas'
        },
        tooltip: {
          pointFormat: '{series.name} <b>R$ {point.y}</b>',
          percentageDecimals: 1
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: '#000000',
              formatter: function () {
                return '<b>' + this.point.name + '</b>: % ' + this.point.porcentagem + ' ';
              }
            }
          }
        },
        credits: { enabled: false },
        series: [{
          type: 'pie',
          name: 'Valor: ',
          data: $scope.pieDataContaReceita
        }]
      });
    
    $scope.configChartContaDespesa = new Highcharts.Chart({
        chart: {
          renderTo: 'chartDespesasConta',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
        },
        title: {
          text: 'Despesas por Contas'
        },
        tooltip: {
          pointFormat: '{series.name} <b>R$ {point.y}</b>',
          percentageDecimals: 1
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              color: '#000000',
              connectorColor: '#000000',
              formatter: function () {
                return '<b>' + this.point.name + '</b>: % ' + this.point.porcentagem + ' ';
              }
            }
          }
        },
        credits: { enabled: false },
        series: [{
          type: 'pie',
          name: 'Valor: ',
          data: $scope.pieDataContaDespesa
        }]
    });
        
    $scope.remover = function(lancamento){
        
        dbFactory.removeLancamento(lancamento).then(function(data){
            swal("Sucesso", "Registro removido!", "success");
            $scope.initLancamentos();                        
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });     
    }
    
    $scope.confirmarPagamento = function(lancamento){
        
        dbFactory.setLancamentoPago(lancamento).then(function(data){
            swal("Sucesso", "Pagamento Confirmado!", "success");
            $scope.initLancamentos();                        
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });     
    }
    
    $scope.carregaFaturas = function(){
        dbFactory.getFaturas($rootScope.anoSelecionado, $rootScope.mesSelecionado, $scope.cartaoSelecionado.descricao).then(function(data){
            $scope.fatura = data;
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });
    }
    
    $scope.carregaCartoes = function(callback){
        dbFactory.getCartoes().then(function(data){
            callback(data);
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });    
    }
    
    $scope.changeCartaoSelecionado = function(cartao){
        $scope.cartaoSelecionado = cartao;
        $scope.labelsChartCartao = ["Utilizado(R$)", "Disponível(R$)"];
        $scope.dataChartCartao = [$scope.cartaoSelecionado.utilizado, parseFloat($scope.cartaoSelecionado.limite - $scope.cartaoSelecionado.utilizado).toFixed(2)];
            
        $scope.carregaFaturas();    
    }
    
    $scope.fecharFatura = function(valor){
        
        var lancamento = {};
        lancamento.ano = $rootScope.anoSelecionado;
        lancamento.categoria = {descricao: "Cartão de crédito", icon: "credit_card"};
        lancamento.conta = $scope.cartaoSelecionado.conta;
        lancamento.descricao = "Fatura " + $scope.cartaoSelecionado.descricao;
        lancamento.mes = $rootScope.mesSelecionado;
        lancamento.pagamentoRealizado = false;
        lancamento.repeticao = "unica";
        lancamento.tipo = "despesa";
        lancamento.valor = valor != undefined ? valor : $scope.cartaoSelecionado.utilizado;
        lancamento.vencimento = moment(""+$rootScope.anoSelecionado+"-"+$rootScope.mesSelecionado+"-"+$scope.cartaoSelecionado.vencimento).toString();
        
        dbFactory.setLancamento(lancamento).then(function (data) {
            
            dbFactory.fechaFatura(lancamento.ano, lancamento.mes, $scope.cartaoSelecionado.descricao).then(function(data){
                swal("Sucesso", "Fatura fechada!", "success");
                $rootScope.carregaAnosExistentes();
                $scope.carregaFaturas();
            },
            function(result){
                swal("Erro", result, "error");
                $scope.isLoading = false;
            });     
            
        }, function (result) {
            swal("Erro", result, "error");
        });
    }
    
    $scope.$watch("pieDataReceita", function (newValue) {
        $scope.configChartReceita.series[0].setData(newValue, true);
    }, true);
    
    $scope.$watch("pieDataDespesa", function (newValue) {
        $scope.configChartDespesa.series[0].setData(newValue, true);
    }, true);
    
    $scope.$watch("pieDataContaReceita", function (newValue) {
        $scope.configChartContaReceita.series[0].setData(newValue, true);
    }, true);
    
    $scope.$watch("pieDataContaDespesa", function (newValue) {
        $scope.configChartContaDespesa.series[0].setData(newValue, true);
    }, true);
    
    $scope.$watch("diasLancamentos", function (newValue) {
        if (newValue != undefined)
            $scope.configChartMovimentacao();
    }, true);
    
    $scope.$watch("valoresPorDia", function (newValue) {
        if (newValue != undefined)
            $scope.configChartMovimentacao();
    }, true);
    
    $scope.$watch("mesSelecionado", function (newValue) {
        $scope.lancamentos = [];
        $scope.initLancamentos();
    }, true);
    
    $scope.$watch("receitasAbertas", function (newValue) {
        $scope.totalReceita = $scope.totalizaPorTipo($scope.lancamentos, 'receita', $scope.receitasAbertas);    
    }, true);
    
    $scope.$watch("despesasAbertas", function (newValue) {
        $scope.totalDespesa = $scope.totalizaPorTipo($scope.lancamentos, 'despesa', $scope.despesasAbertas);
    }, true);
        
    $scope.initLancamentos = function(){
        $scope.lancamentos = [];
        $scope.isLoading = true;
        dbFactory.getLancamentosDetalhes($rootScope.anoSelecionado, $rootScope.mesSelecionado).then(function(data){
            
            if (data.length > 0)
            {
                
                $scope.lancamentos = data;
                
                $scope.lancamentos = data.sort(function (a, b) {
                    return a.vencimento - b.vencimento;
                }); 

                $scope.totalReceita = $scope.totalizaPorTipo($scope.lancamentos, 'receita', $scope.receitasAbertas);
                $scope.totalDespesa = $scope.totalizaPorTipo($scope.lancamentos, 'despesa', $scope.despesasAbertas);

                $scope.totalizaConsolidadosGeral();
                $scope.carregaSaldosProjetados();
                
                $scope.totalizaPorCategoria();
                $scope.totalizaPorConta();

                $scope.getDiasLancamentos();
                $scope.getValorPorDia();

                $scope.isLoading = false;      
            }
            else
            {
                $scope.isLoading = false; 
                $scope.lancamentos = [];
            }
            
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });
        
    }
    $scope.initLancamentos();
    
    $scope.initFaturas = function(){
        $scope.isLoading = true;        
        
        $scope.carregaCartoes(function(data){
            
            $scope.cartoes = data;
            $scope.cartaoSelecionado = $scope.cartoes[0];
            $scope.labelsChartCartao = ["Utilizado(R$)", "Disponível(R$)"];
            $scope.dataChartCartao = [$scope.cartaoSelecionado.utilizado, parseFloat($scope.cartaoSelecionado.limite - $scope.cartaoSelecionado.utilizado).toFixed(2)];
            
            $scope.carregaFaturas();
            
            $scope.isLoading = false;
        });        
    }
    
}]);