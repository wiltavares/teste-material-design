'use strict';
﻿var app = angular.module('app', ['ui.router', 'ng-mfb', 'ui.utils.masks', 'firebase', 'angularMoment', 'chart.js']);

app.run(function ($rootScope, $timeout) {
    $rootScope.$on('$viewContentLoaded', function () {
        $timeout(function () {
            componentHandler.upgradeAllRegistered();
        });
    });
});

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('home', {
            url: '/home',
            templateUrl: '/app/views/home.html',
            controller: 'HomeCtrl'
        })

        .state('lancamentos', {
            url: '/lancamentos',
            templateUrl: '/app/views/lancamentos.html',
            controller: 'LancamentosCtrl'
        })

        .state('cadastro', {
            url: '/cadastro',
            templateUrl: '/app/views/cadastro.html',
            controller: 'CadastroCtrl'
        })

        .state('categorias', {
            url: '/categorias',
            templateUrl: '/app/views/categorias.html',
            controller: 'CategoriaCtrl'
        })
    
        .state('contas', {
            url: '/contas',
            templateUrl: '/app/views/contas.html',
            controller: 'ContasCtrl'
        })
    
        .state('cartoes', {
            url: '/cartoes',
            templateUrl: '/app/views/cartoes.html',
            controller: 'CartoesCtrl'
        })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});

/*app.config(["$locationProvider", function ($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);*/
