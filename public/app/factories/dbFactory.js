app.factory('dbFactory', function($q){
    
    var config = {
      apiKey: "AIzaSyD63-DJJEQ_DtK4KyB5I_s4p4uQw8VQ3Ks",
      authDomain: "sistema-financeiro-pessoal.firebaseapp.com",
      databaseURL: "https://sistema-financeiro-pessoal.firebaseio.com",
      storageBucket: "sistema-financeiro-pessoal.appspot.com",
    };
    firebase.initializeApp(config);
   
    var service = {};
    var database = firebase.database();
    
    service.getCategorias = function(){
        var deferred = $q.defer();
        var categorias = [];
        
        database.ref('categorias/').once('value').then(function(item){
            categorias = item.val();
            deferred.resolve(categorias);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;    
    }
    
    service.getContas = function(){
        var deferred = $q.defer();
        var contas = [];
        
        database.ref('contas/').once('value').then(function(item){
            contas = item.val();
            deferred.resolve(contas);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;    
    }
    
    service.getCartoes = function(){
        var itens = [];
        var item = {};
        var deferred = $q.defer();
        
        database.ref('cartoes/').once('value').then(function(cartoes){
            
            cartoes.forEach(function(cartao){
                item = cartao.val();
                item.key = cartao.key;
                itens.push(item);
            });
            deferred.resolve(itens);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;        
    }
    
    service.getAnosExistentes = function(){
        var deferred = $q.defer();
        var anos = {};
        
        database.ref('lancamentos/').once('value').then(function(item){
            anos = Object.keys(item.val());
            deferred.resolve(anos);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;
    }
    
    service.getNomeMes = function(mes){
        
        switch (mes) {
          case '1': return 'Janeiro'
            break;
          case '2': return 'Fevereiro'
            break;
          case '3': return 'Março'
            break;
          case '4': return 'Abril'
            break;
          case '5': return 'Maio'
            break;
          case '6': return 'Junho'
            break;
          case '7': return 'Julho'
            break;
          case '8': return 'Agosto'
            break;
          case '9': return 'Setembro'
            break;
          case '10': return 'Outubro'
            break;
          case '11': return 'Novembro'
            break;
          case '12': return 'Dezembro'
            break;
          default:  return ''
        }
  }
      
    service.getLancamentos = function(anoSelecionado){
        
        var item = {creditos: 0,  debitos: 0, totais: 0};
        var itens = [];
        var deferred = $q.defer();
        
        database.ref('lancamentos/').once('value').then(function(lancamentos){
               
          lancamentos.forEach(function(ano){;
              
              if (ano.key == anoSelecionado){
                
                ano.forEach(function(mes){
                  item.numeroMes = mes.key;    
                  item.descricaoMes = service.getNomeMes(mes.key);

                  mes.forEach(function(lancamento){

                    if(lancamento.val().tipo == 'receita'){
                      item.creditos = item.creditos + lancamento.val().valor;
                      item.totais = item.totais + lancamento.val().valor;
                    }
                    else{
                      item.debitos = item.debitos + lancamento.val().valor;
                      item.totais = item.totais - lancamento.val().valor;
                    }
                  });
                    
                  itens.push(item);
                  item = {creditos: 0,  debitos: 0, totais: 0};        
                });  
              }
          });
          
          deferred.resolve(itens); 
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });   
        
        return deferred.promise;
    }  
        
    service.getLancamentosDetalhes = function(anoSelecionado, mesSelecionado){
        
        var itens = [];
        var item = {};
        var deferred = $q.defer();
        
        database.ref('lancamentos/'+ anoSelecionado +'/'+ mesSelecionado).once('value').then(function(lancamentos){
            
            lancamentos.forEach(function(lancamento){
                item = lancamento.val();
                item.key = lancamento.key;
                item.vencimento = Date.parse(item.vencimento);
                itens.push(item);        
            });
          deferred.resolve(itens);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });   
        return deferred.promise;
    }
    
    service.getFaturas = function(anoSelecionado, mesSelecionado, cartao){
        var deferred = $q.defer();
        var itens = [];
        var item = {};
        
        database.ref('faturas/'+ anoSelecionado +'/'+ mesSelecionado +'/'+ cartao).once('value').then(function(data){
            
            data.forEach(function(fatura){
                item = fatura.val();
                
                if(item.dataCompra != undefined)
                    item.dataCompra = Date.parse(item.dataCompra); 
                itens.push(item); 
                
            });
            
          deferred.resolve(itens);
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });   
        return deferred.promise;    
    }
    
    service.setLancamento = function(item){
        var deferred = $q.defer();
        
        database.ref().child('lancamentos/' + item.ano + '/' +  item.mes).push(item).then(function (result) {
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;
    }
    
    service.setLancamentoPago = function(item){
        
        var deferred = $q.defer();
        
        delete item.$$hashKey;
        
        database.ref('lancamentos/' + item.ano + '/' +  item.mes + '/' + item.key + '/pagamentoRealizado').set(true).then(function (result) {
            deferred.resolve("Pagamento Confirmado!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;
    }
    
    service.setLancamentoFatura = function(item){
        var deferred = $q.defer();
        
        database.ref().child('faturas/' + item.ano + '/' +  item.mes + '/' + item.cartao.descricao).push(item).then(function (result) {
            
            service.atualizaLimiteCartao(item.cartao.key, 'debito', item.valor);
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;    
    }
    
    service.removeLancamento = function(item){
        var deferred = $q.defer();
        var lancamento = database.ref('lancamentos/'+item.ano+'/'+item.mes+'/'+item.key);
        
        if (lancamento != undefined)
        {
            lancamento.remove();
             deferred.resolve(true);
        }
        else
        {
            deferred.reject("Falha ao executar a instrução, motivo: Lançamento não encontrado");    
        }
        
        return deferred.promise;
        
    }
    
    service.setCategoria = function(item){
        var deferred = $q.defer();
        
        database.ref().child('categorias/').push(item).then(function (result) {
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;    
    }
    
    service.setConta = function(item){
        var deferred = $q.defer();
        
        database.ref().child('contas/').push(item).then(function (result) {
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;    
    }
    
    service.setCartao = function(item){
        var deferred = $q.defer();
        
        database.ref().child('cartoes/').push(item).then(function (result) {
            deferred.resolve("Registro inserido!");
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;     
    }
    
    service.atualizaLimiteCartao = function(key, operacao, valor){
        var deferred = $q.defer();
        
         database.ref('cartoes/' + key + '/utilizado').once('value').then(function (result) {
            debugger;
            var utilizado = result.val();
            utilizado = utilizado + valor;
             
            database.ref('cartoes/' + key + '/utilizado').set(utilizado);
            deferred.resolve("Limite do cartão atualizado");
             
        }).catch(function (result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });
        return deferred.promise;
    }
    
    service.fechaFatura = function(anoSelecionado, mesSelecionado, cartao){
        var deferred = $q.defer();
        
        database.ref('faturas/'+ anoSelecionado +'/'+ mesSelecionado +'/'+ cartao +'/fechada').set(true).then(function(result){
            deferred.resolve("Fechamento Confirmado!");
        })
        .catch(function(result){
            deferred.reject("Falha ao executar a instrução, motivo:\n "+result.message);
        });   
        return deferred.promise;    
    }
    
    return service;
});