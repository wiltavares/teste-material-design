'use strict';
app.controller("ContasCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {
    
    $scope.contas = [];
    
    $scope.salvar = function(item){
        
        $scope.item = angular.copy(item);
        
        if ($scope.item != undefined)
        {
            
            dbFactory.setConta($scope.item).then(function (data) {

                swal("Sucesso", "Registro inserido!", "success");
                $scope.limpar();
                $scope.init();
            }, function (result) {
                swal("Erro", result, "error");
            });    
        }
    }
    
    $scope.limpar = function(){
        $scope.item = {};
    }
    
    $scope.init = function(){
        $scope.isLoading = true;
        dbFactory.getContas().then(function(data){ 
            
            $scope.contas = data; 
            $scope.isLoading = false;
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });    
    };
    $scope.init();
    
    
}]);