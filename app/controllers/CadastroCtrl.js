'use strict';
app.controller("CadastroCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {
    
    $scope.item = {};
    $scope.categorias = [];
    $scope.contas = [];
    $scope.cartoes = [];
        
    $scope.limpar = function () {
        $scope.item = {};        
    }
    
    $scope.salvarLancamentoCartao = function(item){
        $scope.item = angular.copy(item);
        
        var vencimento = moment($scope.item.dataCompra);
        
        if (vencimento.get('date') > $scope.item.cartao.fechamento)
        {
            vencimento = vencimento.month(vencimento.month() + 1);
            
            $scope.item.ano = vencimento.year();
            $scope.item.mes = vencimento.month()+1;
        }
        
        dbFactory.setLancamentoFatura($scope.item).then(function (data) {

            if ($scope.item.repeticao == 'parcelada')
            {
                $scope.salvarLancamentoCartaoParcelado($scope.item);        
            }

            swal("Sucesso", "Registro inserido!", "success");

            $scope.form.$submitted = false;
            $scope.limpar();
            $rootScope.carregaAnosExistentes();
        }, function (result) {
            swal("Erro", result, "error");
        });  
    }
    
    $scope.salvarLancamentoCartaoParcelado = function(item){
        $scope.item = angular.copy(item);
        
        var quant = $scope.item.numParcelas  - $scope.item.parcelaInicial;
        
        for (var i = 1; i <= quant; i++)
        {                        
            $scope.item.ano = $scope.item.mes == 12 ? $scope.item.ano + 1 : $scope.item.ano;
            $scope.item.mes = $scope.item.mes == 12 ? 1 : $scope.item.mes + 1;
                                    
            dbFactory.setLancamentoFatura($scope.item).then(function (data) {

            }, function (result) {
                swal("Erro", result, "error");
            });
        }    
    }
    
    $scope.salvar = function (item) {
        
        $scope.form.$submitted = true;
        
        if ($scope.form.$valid)
        {
            $scope.item = angular.copy(item);
                           
            if ($scope.item.tipo != 'cartao')
            {
                $scope.item.ano = item.vencimento.getFullYear();
                $scope.item.mes = item.vencimento.getMonth()+1;
                $scope.item.vencimento = $scope.item.vencimento.toString();
                
                $scope.item.conta = $scope.item.conta.descricao;

                if ($scope.item.pagamentoRealizado == undefined)
                {
                    $scope.item.pagamentoRealizado = false;
                }

                dbFactory.setLancamento($scope.item).then(function (data) {

                    if ($scope.item.repeticao == 'parcelada')
                    {
                        $scope.salvarParcelado($scope.item);        
                    }

                    swal("Sucesso", "Registro inserido!", "success");

                    $scope.form.$submitted = false;
                    $scope.limpar();
                    $rootScope.carregaAnosExistentes();
                }, function (result) {
                    swal("Erro", result, "error");
                });   
            }
            else
            {
                $scope.item.ano = item.dataCompra.getFullYear();
                $scope.item.mes = item.dataCompra.getMonth()+1;
                $scope.item.dataCompra = $scope.item.dataCompra.toString();
                
                $scope.salvarLancamentoCartao($scope.item);
            }
                
        }
    };
    
    $scope.salvarParcelado = function(item){
        $scope.item = angular.copy(item);
        
        var quant = $scope.item.numParcelas  - $scope.item.parcelaInicial;
        
        for (var i = 1; i <= quant; i++)
        {
            debugger;
            var vencimento = moment($scope.item.vencimento);
            var proximoVencimento = moment($scope.item.vencimento);
            
            if ($scope.item.formaRepeticao == 'dia')
            {
                proximoVencimento = vencimento.day(vencimento.day() + 1);   
            }
            else if ($scope.item.formaRepeticao == 'semana')
            {
                proximoVencimento = vencimento.day(vencimento.day() + 7);   
            }
            else if ($scope.item.formaRepeticao == 'mes')
            {
                proximoVencimento = vencimento.month(vencimento.month() + 1); 
            }
            else if ($scope.item.formaRepeticao == 'ano')
            {
                proximoVencimento = vencimento.year(vencimento.year() + 1);    
            }
            
            $scope.item.ano = proximoVencimento.year();
            $scope.item.mes = proximoVencimento.month()+1;
                        
            $scope.item.vencimento = proximoVencimento.toString();
            
            dbFactory.setLancamento($scope.item).then(function (data) {

            }, function (result) {
                swal("Erro", result, "error");
            });
        }
    }
    
    $scope.carregaCategorias = function(){
        dbFactory.getCategorias().then(function(data){ 
            
            $scope.categorias = data; 
        },
        function(result){
            swal("Erro", result, "error");
        });
    };
    
    $scope.carregaContas = function(){
        dbFactory.getContas().then(function(data){ 
            
            $scope.contas = data; 
        },
        function(result){
            swal("Erro", result, "error");
        });
    };
    
    $scope.carregaCartoes = function(){
        dbFactory.getCartoes().then(function(data){ 
            
            $scope.cartoes = data;
            console.log($scope.cartoes);
        },
        function(result){
            swal("Erro", result, "error");
        });
    };
    
    $scope.init = function () {
        $rootScope.showBarActions = true;
        
        $scope.carregaCategorias();
        $scope.carregaContas();
        $scope.carregaCartoes();
    }
    $scope.init();
}]);