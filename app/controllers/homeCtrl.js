'use strict';
app.controller("HomeCtrl", ['$scope', '$rootScope', '$state', 'dbFactory', function ($scope, $rootScope, $state, dbFactory) {

    $scope.meses = [];

    $scope.init = function(){
        $scope.isLoading = true;
        dbFactory.getLancamentos($rootScope.anoSelecionado).then(function(data){
            $scope.meses = data; 
            $scope.isLoading = false;
        },
        function(result){
            swal("Erro", result, "error");
            $scope.isLoading = false;
        });
    }
    $scope.init();
    
    $scope.detalhar = function(mes){
        $rootScope.mesSelecionado = mes.numeroMes;
        $state.go('lancamentos');
    }
    
    $scope.$watch("anoSelecionado", function (newValue) {
        $scope.meses = [];
        $scope.init();
    }, true);

}]);
